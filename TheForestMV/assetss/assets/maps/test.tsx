<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.5.0" name="test" tilewidth="63" tileheight="59" tilecount="10" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="1">
  <properties>
   <property name="name" value="start"/>
  </properties>
  <image width="63" height="59" source="../respawn/1.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="name" value="boots"/>
  </properties>
  <image width="32" height="12" source="../items/01.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="name" value="lastPoint"/>
  </properties>
  <image width="32" height="12" source="../items/02.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="name" value="gravity"/>
  </properties>
  <image width="32" height="32" source="../items/gravity.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="name" value="fast"/>
  </properties>
  <image width="32" height="32" source="../items/gun.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="name" value="avion"/>
  </properties>
  <image width="32" height="32" source="../items/0001.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="name" value="slider"/>
  </properties>
  <image width="32" height="32" source="../items/0002.png"/>
 </tile>
 <tile id="12">
  <properties>
   <property name="name" value="roca"/>
  </properties>
  <image width="32" height="32" source="../items/0003.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="name" value="torreta"/>
  </properties>
  <image width="32" height="32" source="../items/0004.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="name" value="plataforma"/>
  </properties>
  <image width="32" height="32" source="../individualitems/1.png"/>
 </tile>
</tileset>

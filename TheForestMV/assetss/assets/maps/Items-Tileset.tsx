<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.5.0" name="Items-Tileset " tilewidth="128" tileheight="64" tilecount="3" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="color" value="green"/>
   <property name="name" value="llave"/>
  </properties>
  <image width="32" height="32" source="../items/key.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="color" value="green"/>
   <property name="name" value="Lock"/>
  </properties>
  <image width="32" height="32" source="../items/lock.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="color" value="green"/>
   <property name="name" value="llave"/>
  </properties>
  <image width="128" height="64" source="../key-icon.png"/>
 </tile>
</tileset>

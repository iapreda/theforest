package character;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameClasses.Bullet;
import gameClasses.Parameters;
import gameClasses.Solid;
import gameConfiguration.LevelScreen;
import gameStructure.BaseActor;

/**
 * Clase publica en la que el personaje cobra vida. En esta clase se instancian
 * las variables para que el personaje dispare y salte. Tambien se agregan
 * animaciones al mismo.
 * 
 * @author Iulian Preda.
 *
 */
public class Character extends BaseActor {

	@SuppressWarnings("rawtypes")
	public Animation idle;
	@SuppressWarnings("rawtypes")
	public Animation walk;
	@SuppressWarnings("rawtypes")
	public Animation jump;
	public ArrayList<Bullet> bulletss;
	public int bulletNumber;
	public int actualBullet;
	public int bulletDirection;
	public float bulletCooldownTimer;
	public float bulletCooldown;
	public float collisionSpeed;
	public float healtPoints;
	public float walkAcceleration;
	public float maxWalkSpeed;
	public float jumpSpeed;
	public float footHeight;
	public float footOffset;
	public float immortalTime;
	public float invulnerableTime;

	public LevelScreen nivel;
	public BaseActor foots;
	public ArrayList<Solid> solid;

	/**
	 * Constructor de la clase. Se crean animaciones a partir de archivos
	 * individuales. Tambien se inicializan algunas variables declaradas
	 * previamente.
	 * 
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public Character(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s);

		solid = new ArrayList<Solid>();

		this.nivel = nivel;
		String[] idleFileNames = { "assets/pnjafk/1.png", "assets/pnjafk/2.png", "assets/pnjafk/3.png",
				"assets/pnjafk/4.png", "assets/pnjafk/5.png", "assets/pnjafk/6.png", "assets/pnjafk/7.png",
				"assets/pnjafk/8.png", "assets/pnjafk/9.png", "assets/pnjafk/10.png", "assets/pnjafk/11.png",
				"assets/pnjafk/12.png", "assets/pnjafk/13.png", "assets/pnjafk/14.png", "assets/pnjafk/15.png",
				"assets/pnjafk/16.png", "assets/pnjafk/17.png", "assets/pnjafk/18.png" };
		idle = loadAnimationFromFiles(idleFileNames, 0.1f, true);

		String[] walkFileNames = { "assets/pnjwalk/1.png", "assets/pnjwalk/2.png", "assets/pnjwalk/3.png",
				"assets/pnjwalk/4.png", "assets/pnjwalk/5.png", "assets/pnjwalk/6.png", "assets/pnjwalk/7.png",
				"assets/pnjwalk/8.png", "assets/pnjwalk/9.png", "assets/pnjwalk/10.png", "assets/pnjwalk/11.png",
				"assets/pnjwalk/12.png", "assets/pnjwalk/13.png", "assets/pnjwalk/14.png", "assets/pnjwalk/15.png",
				"assets/pnjwalk/16.png", "assets/pnjwalk/17.png", "assets/pnjwalk/18.png", "assets/pnjwalk/19.png",
				"assets/pnjwalk/20.png", "assets/pnjwalk/21.png", "assets/pnjwalk/22.png", "assets/pnjwalk/23.png",
				"assets/pnjwalk/24.png", };
		walk = loadAnimationFromFiles(walkFileNames, 0.1f, true);

		String[] jumpFileNames = { "assets/pnjjump/1.png", "assets/pnjjump/2.png", "assets/pnjjump/3.png",
				"assets/pnjjump/4.png", "assets/pnjjump/5.png", "assets/pnjjump/6.png", "assets/pnjjump/1.png",
				"assets/pnjjump/7.png", "assets/pnjjump/8.png", "assets/pnjjump/9.png", "assets/pnjjump/10.png",
				"assets/pnjjump/11.png", "assets/pnjjump/12.png", "assets/pnjjump/11.png", "assets/pnjjump/10.png",
				"assets/pnjjump/9.png", "assets/pnjjump/8.png", "assets/pnjjump/7.png", "assets/pnjjump/6.png",
				"assets/pnjjump/5.png", "assets/pnjjump/4.png", "assets/pnjjump/3.png", "assets/pnjjump/2.png",
				"assets/pnjjump/1.png" };
		jump = loadAnimationFromFiles(jumpFileNames, 0.1f, true);

		setBoundaryPolygon(10);

		maxWalkSpeed = 200;
		walkAcceleration = 270;
		jumpSpeed = 300;
		footHeight = 6;
		footOffset = 10;
		collisionSpeed = 1400;
		healtPoints = 120;
		immortalTime = 1;
		invulnerableTime = 0;

		actualBullet = 0;
		bulletNumber = 30;
		bulletCooldown = 0.5f;
		bulletCooldownTimer = 0;

		bulletss = new ArrayList<Bullet>();

		for (int i = 0; i < bulletNumber; i++) {
			bulletss.add(new Bullet(0, 0, s, nivel));
		}

		new ShapeRenderer();

		foots = new BaseActor(0, 0, s);

		foots.setSize(this.getWidth() - footOffset, footHeight);
		foots.setBoundaryRectangle();

	}

	/**
	 * Metodo para actualizar el estado del personaje. Tambien se agregan
	 * caractersticas como por ejemplo, la accion de disparar.
	 */
	@SuppressWarnings("unchecked")
	public void act(float dt) {

		super.act(dt);

		if (this.invulnerableTime > 0) {
			this.invulnerableTime -= dt;
		}

		if (Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.LEFT)) {
			accelerationVec.add(-walkAcceleration, 0);
			bulletDirection = -1;

		}

		if (Gdx.input.isKeyPressed(Keys.D) || Gdx.input.isKeyPressed(Keys.RIGHT)) {
			accelerationVec.add(walkAcceleration, 0);
			bulletDirection = 0;
		}

		if (Gdx.input.isKeyPressed(Keys.E) && this.bulletCooldownTimer <= 0) {

			if (bulletDirection == 0) {
				this.disparar(1);

			} else {
				this.disparar(-1);

			}

		}
		if (!Gdx.input.isKeyPressed(Keys.E)) {
			setAnimation(idle);
		}
		if (bulletCooldownTimer > 0) {
			this.bulletCooldownTimer -= dt;
		}

		/*
		 * Detecto si se han dejado de pulsar teclas. Si no hay ninguna tecla
		 * presionada, el personaje se para de golpe.
		 */
		if (!Gdx.input.isKeyPressed(Keys.A) && !Gdx.input.isKeyPressed(Keys.D)) {
			velocityVec.x = 0;

		}

		if (Gdx.input.isKeyPressed(Keys.A) && Gdx.input.isKeyPressed(Keys.D)) {
			velocityVec.x = 0;

		}

		accelerationVec.add(0, -Parameters.getGravedad());

		velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
		velocityVec.x = MathUtils.clamp(velocityVec.x, -maxWalkSpeed, maxWalkSpeed);

		moveBy(velocityVec.x * dt, velocityVec.y * dt);

		accelerationVec.set(0, 0);

		foots.setPosition(this.getX() + footOffset / 2, this.getY() - footHeight);

		/*
		 * Detecto si el personaej esta en objetos solidos. En funcion del estado, se
		 * utilizara una animacion u otra.
		 */

		if (this.isOnSolid()) {
			if (velocityVec.x == 0) {
				setAnimation(idle);

			} else {
				setAnimation(walk);
			}

		} else {
			setAnimation(jump);

		}

		if (velocityVec.x > 0) {
			setScaleX(1);
		} else if (velocityVec.x < 0) {
			setScaleX(-1);
		}

		alignCamera();

		boundToWorld();
	}

	/**
	 * Metodo para dibujar al personaje en pantalla. Tambien se le agrega una
	 * hitbox.
	 *
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {

		super.draw(batch, parentAlpha);
		batch.end();

		if (this.getBoundaryPolygon() != null) {
			float[] vertices = new float[this.getBoundaryPolygon().getVertices().length];

			for (int i = 0; i < vertices.length / 2; i++) {
				vertices[2 * i] = this.getBoundaryPolygon().getVertices()[2 * i] + this.getX();
				vertices[2 * i + 1] = this.getBoundaryPolygon().getVertices()[2 * i + 1] + this.getY();
			}
		}

		batch.begin();

	}

	public boolean belowOverlaps(BaseActor actor) {
		return foots.overlaps(actor);
	}

	/**
	 * Metodo para detectar si el personaje esta en solidos o no.
	 * 
	 * @return
	 */
	public boolean isOnSolid() {

		for (BaseActor actor : BaseActor.getList(getStage(), "gameClasses.Solid")) {
			Solid solid = (Solid) actor;
			if (belowOverlaps(solid) && solid.isEnabled()) {

				return true;
			}
		}

		return false;

	}

	/**
	 * Metodo para que el personaje salte. Tambien se le agrega un sonido.
	 */
	public void salto() {

		velocityVec.y = jumpSpeed;
		nivel.JumpSound();
	}

	/**
	 * Metodo para que el personaje dispare en una direccion.
	 * 
	 * @param direccion
	 */
	public void disparar(int direccion) {

		bulletss.get(actualBullet).generarOrbe(direccion);
		actualBullet++;
		actualBullet = actualBullet % bulletNumber;
		bulletCooldownTimer = bulletCooldown;
		nivel.ShotSound();

	}

	/**
	 * Metodo para que el personaje reciba dano.
	 * 
	 * @param dano
	 */
	public void dano(int dano) {
		if (this.invulnerableTime <= 0) {
			this.healtPoints -= dano;
			this.invulnerableTime = this.immortalTime;
		}

	}

	/**
	 * Metodo para comprobar si el jugador esta cayendo.
	 * 
	 * @return
	 */
	public boolean isFalling() {
		return (velocityVec.y < 0);
	}

	/**
	 * Metodo para comprobar si el jugador esta saltando.
	 * 
	 * @return
	 */
	public boolean isJumping() {
		return (velocityVec.y > 0);
	}

	/**
	 * Getters y Setters de variables que se utilizan en otras clases.
	 * 
	 * @see gameConfiguration.LevelScreen
	 * @return
	 */
	public float getBulletCooldown() {
		return bulletCooldown;
	}

	public void setBulletCooldown(float bulletCooldown) {
		this.bulletCooldown = bulletCooldown;
	}

	public float getHealtPoints() {
		return healtPoints;
	}

}

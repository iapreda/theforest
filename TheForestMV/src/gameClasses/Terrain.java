package gameClasses;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Clase publica que extiende de solidos. Establece una caja de colisiones a los
 * terrenos.
 *
 * @author Iulian Preda.
 *
 */
public class Terrain extends Solid {
	ShapeRenderer shapeRenderer;

	public Terrain(float x, float y, float width, float height, Stage s) {
		super(x, y, width, height, s);

		shapeRenderer = new ShapeRenderer();
	}

}

package gameClasses;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameConfiguration.LevelScreen;
import gameStructure.BaseActor;

/**
 * Clase publica que establece unos parametros o atributos a uno de los enemigos
 * que hay dentro del juego.
 * 
 * @author Iulian Preda
 *
 */
public class EnemyFliyingBomb extends Enemy {

	// Declaracion de variables que utiliza esta clase.

	@SuppressWarnings("rawtypes")
	public Animation flying;
	public ArrayList<EnemyBullet> bullet;
	public BaseActor foot;
	public BaseActor head;

	public float maxSpeed;
	public float acceleration;
	public float shotTime;
	public float shotCount;

	public int numberBullet;
	public int actualBullet;
	public int aggro;
	public int direction;


	/**
	 * Constructor de la clase. Inicializo animaciones y variables.
	 * 
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public EnemyFliyingBomb(float x, float y, Stage s, LevelScreen nivel) {

		super(x, y, s, nivel);

		EnemyBullet bala;

		String[] walkFileNames = { "assets/bomb/1.png", "assets/bomb/2.png", "assets/bomb/3.png", "assets/bomb/4.png",
				"assets/bomb/5.png", "assets/bomb/6.png", "assets/bomb/7.png", "assets/bomb/8.png", "assets/bomb/9.png",
				"assets/bomb/10.png", "assets/bomb/11.png", "assets/bomb/12.png", "assets/bomb/13.png",
				"assets/bomb/14.png", "assets/bomb/15.png" };
		flying = loadAnimationFromFiles(walkFileNames, 0.1f, true);

		maxSpeed = 100;
		acceleration = 900;
		direction = 1;
		aggro = 400;
		shotTime = 1;
		shotCount = 0;
		numberBullet = 5;
		actualBullet = 0;
		bullet = new ArrayList<EnemyBullet>();

		foot = new BaseActor(0, 0, s);
		foot.setSize(this.getWidth() / 6, 30);
		foot.setBoundaryRectangle();
		setFootSize();

		head = new BaseActor(0, 0, s);

		head.setSize(this.getWidth() / 6, 30);
		head.setBoundaryRectangle();

		for (int i = 0; i < numberBullet; i++) {
			System.out.println(numberBullet);
			bala = new EnemyBullet(0, 0, s, nivel);
			bullet.add(bala);
		}
		setHeadSize();

		
		
	}

	/**
	 * Metodo para actualizar el funcionamiento de la clase.
	 */
	@SuppressWarnings("static-access")
	@Override
	public void act(float dt) {

		super.act(dt);


		boolean var1 = false;
		boolean var2 = false;
		
		if (this.isAlive) {

			if (Math.sqrt(Math.pow(this.getX() - level.jugador.getX(), 2)
					+ Math.pow(this.getY() - level.jugador.getY(), 2)) < aggro) {
				if (this.shotCount <= 0) {
					shot();
				} else {
					shotCount -= dt;
				}
			}

		}
		for (Solid solido : level.getSolidos()) {
			if (!foot.overlaps(solido)) {
				var1 = true;
			}
			if (head.overlaps(solido)) {
				var2 = true;
			}

		}

		if (var2) {
			direction *= -1;

		}
		if (direction > 0) {
			setScaleX(-1);
		} else {
			setScaleX(1);
		}

		velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
		moveBy(direction * maxSpeed * dt, velocityVec.x * dt);

		setFootSize();
		setHeadSize();

		boundToWorld();

	}

	/**
	 * Metodo para asignar una hitbox.
	 */
	private void setHeadSize() {
		if (direction == -1) {
			head.setPosition(this.getX() - 15, this.getY() + 10);
		} else {
			head.setPosition(this.getX() + this.getWidth() + 15 - this.getWidth() / 6, this.getY() + 10);
		}

	}

	/**
	 * Metodo para asignar una hitbox.
	 */
	private void setFootSize() {
		if (direction == -1) {
			foot.setPosition(this.getX() + 15, this.getY() - 20);
		} else {
			foot.setPosition(this.getX() + this.getWidth() - 15 - this.getWidth() / 6, this.getY() - 20);
		}

	}

	/**
	 * Metodo para hacer que el enemigo dispare.
	 */
	@SuppressWarnings("static-access")
	private void shot() {
		this.shotCount = this.shotTime;

		float x = this.getX() - level.jugador.getX();
		float y = this.getY() - level.jugador.getY();
		Vector2 vector = new Vector2(x, y).nor();
		bullet.get(actualBullet).disparar(vector.x * -1, vector.y * -1, this.getX(), this.getY());
		actualBullet++;
		actualBullet = actualBullet % numberBullet;

	}

}

package gameClasses;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import gameStructure.BaseActor;


/**
 * Clase publica que agrega una textura de a un objeto que el jugador puede
 * coger y utilizar. Tambien se agrega una action para que el objeto rote.
 * 
 * @author Iulian Preda
 *
 */
public class GravityPerk extends BaseActor {

	public GravityPerk(float x, float y, Stage s) {
		super(x, y, s);
		loadTexture("assets/items/gravity.png");

		rotateBy(10);

		Action tilt = Actions.sequence(Actions.rotateBy(-20, 0.2f), Actions.rotateBy(20, 0.5f));

		addAction(Actions.forever(tilt));
	}
}

package gameClasses;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameConfiguration.LevelScreen;
import gameStructure.BaseActor;

/**
 * Clase publica creada para manejar todos los enemigos de forma conjunta. De
 * esta clase heredaran todos los enemigos que quieran tener un patron en
 * conjunto.
 * 
 * En esta clase se encuentran variables para definir el nivel en el que se
 * encuentra el enemigo, la vida del mismo y el estado.
 * 
 * @author Iulian Preda
 *
 */
public class Enemy extends BaseActor {

	public boolean isAlive;
	protected LevelScreen level;
	int healt;

	/**
	 * Constructor de la clase para iniciar las variables y el estado del mismo.
	 * 
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public Enemy(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s);
		this.level = nivel;
		isAlive = true;
		healt = 50;
	}

	/**
	 * Metodo para inflingir una cantidad exacta de dano al enemigo.
	 * 
	 * @param danoRecibido
	 */
	public void dano(int danoRecibido) {
		this.healt -= danoRecibido;
		if (healt >= 1) {
			this.isAlive = true;
		} else {
			this.isAlive = false;
		}
	}

	/**
	 * Metodo que dibuja al enemigo en caso de estar vivo.
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if (isAlive)
			super.draw(batch, parentAlpha);
	}

}

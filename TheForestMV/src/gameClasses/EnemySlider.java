package gameClasses;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameConfiguration.LevelScreen;
import gameStructure.BaseActor;

/**
 * Clase publica que hereda de Enemigo.
 * 
 * En esta clase se declaran las variables necesarias para que el enemigo
 * funcione de manera optima.
 * 
 * @author Iulian Preda
 *
 */
public class EnemySlider extends Enemy {
	public int direction;
	@SuppressWarnings("rawtypes")
	public Animation walking;
	public float speed;
	public BaseActor lowerHitbox;
	public BaseActor highetHitbox;

	/**
	 * Constructor de la clase donde se incializan las variables. En el constructor
	 * tambien se crea una animacion para que el enemigo tenga movimiento fluido.
	 * 
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public EnemySlider(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s, nivel);

		String[] walkFileNames = { "assets/deslizador/1.png", "assets/deslizador/2.png",
				"assets/deslizador/3.png", "assets/deslizador/4.png", "assets/deslizador/5.png",
				"assets/deslizador/6.png", "assets/deslizador/7.png", "assets/deslizador/8.png",
				"assets/deslizador/9.png", "assets/deslizador/10.png", "assets/deslizador/11.png",
				"assets/deslizador/12.png", "assets/deslizador/13.png" };
		walking = loadAnimationFromFiles(walkFileNames, 0.1f, true);
		speed = 100;
		direction = -1;

		lowerHitbox = new BaseActor(0, 0, s);

		lowerHitbox.setSize(this.getWidth() / 6, 30);
		lowerHitbox.setBoundaryRectangle();
		adjustLowerHitbox();

		highetHitbox = new BaseActor(0, 0, s);

		highetHitbox.setSize(this.getWidth() / 6, 30);
		highetHitbox.setBoundaryRectangle();

		adjustHigherHitbox();

	}

	/**
	 * Metodo que verifica si el enemigo se encuentra habilitado. Si se encuentra
	 * habilitado, se dibujara en pantalla. Si no se encuentra habilidado, no
	 * aparecera.
	 * 
	 * En este metodo tambien se encuentra un IF que controla la colision con el
	 * terreno.
	 */
	@Override
	public void act(float dt) {

		super.act(dt);
		boolean pisa = false;
		boolean cabezazo = false;

		for (Solid solido : level.getSolidos()) {
			if (lowerHitbox.overlaps(solido)) {
				pisa = true;
			}
			if (highetHitbox.overlaps(solido)) {
				cabezazo = true;
			}

		}

		if (cabezazo || !pisa) {
			direction *= -1;

		}
		if (direction > 0) {
			setScaleX(-1);
		} else {
			setScaleX(1);
		}

		moveBy(direction * speed * dt, velocityVec.y * dt);

		adjustLowerHitbox();
		adjustHigherHitbox();
	}

	/**
	 * Metodo para dibujar una hitbox en el enemigo.
	 */
	private void adjustHigherHitbox() {
		if (direction == -1) {
			highetHitbox.setPosition(this.getX() - 15, this.getY() + 10);
		} else {
			highetHitbox.setPosition(this.getX() + this.getWidth() + 15 - this.getWidth() / 6, this.getY() + 10);
		}

	}

	/**
	 * Metodo para dibujar una hitbox en el enemigo.
	 */
	private void adjustLowerHitbox() {
		if (direction == -1) {
			lowerHitbox.setPosition(this.getX() + 15, this.getY() - 20);
		} else {
			lowerHitbox.setPosition(this.getX() + this.getWidth() - 15 - this.getWidth() / 6, this.getY() - 20);
		}

	}

}

package gameClasses;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameConfiguration.LevelScreen;
import gameStructure.BaseActor;

/**
 * Clase para dar atributos al proyectil.
 * 
 * @author Iulian Preda.
 *
 */

public class Bullet extends BaseActor {

	public LevelScreen level;
	public float bulletCooldown; // cronometro que marca la duraci�n del orbe
	public float bulletSpeed; // variable que marca la velocidad del orbe
	public float bulletNumber; // variable contador
	public int bulletDirection;// direcc�n en la que el orbe apunta
	public boolean enabled;

	/**
	 * Constructor de la clase. Inicio las variables.
	 * 
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public Bullet(float x, float y, Stage s, LevelScreen nivel) {

		super(x, y, s);
		this.level = nivel;
		bulletCooldown = 10;
		bulletSpeed = 900;

		loadTexture("assets/items/1.png"); // le doy una textura al orbe para que se pueda identificar en pantalla
	}

	/**
	 * Metodo que verifica si el orbe-proyectil se encuentra habilitado. Si se
	 * encuentra habilitado, se dibujara en pantalla. Si no se encuentra habilidado,
	 * no aparecera.
	 */
	@Override
	public void act(float dt) {
		if (enabled) {// solo calcularemos si el proyecti est� activo
			super.act(dt);
			this.rotateBy(-10 * bulletDirection);
			moveBy(velocityVec.x * dt, velocityVec.y * dt);
			bulletNumber -= dt;
			for (Enemy enemigo : level.enemy) {
				if (enemigo.isAlive && this.enabled && this.overlaps(enemigo)) {
					enemigo.dano(20);
					this.enabled = false;
				}
				for (Solid solido : level.getSolidos()) {
					if (this.overlaps(solido)) {
						this.enabled = false;
					}
				}
			}

			if (bulletNumber <= 0) {
				enabled = false;
			}

		}

	}

	/**
	 * Metodo para dibujar el proyectil en pantalla. Solo se vera el proyectil si
	 * esta activado
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {

		if (enabled)
			super.draw(batch, parentAlpha);
	}

	/**
	 * Metodo que genera el proyectil que aparecera en pantalla. En este metodo se
	 * encuentran parametros como la posicion en pantalla y la direccion a la que se
	 * desplazara. Tambien hay una variable interruptor.
	 * 
	 * @param direction
	 */
	@SuppressWarnings("static-access")
	public void generarOrbe(int direction) {

		this.enabled = true;
		this.setPosition(level.jugador.getX(), level.jugador.getY() + 7);
		bulletNumber = bulletCooldown;
		this.velocityVec.set(bulletSpeed * direction, 0);
		this.bulletDirection = direction;

	}

}

package gameClasses;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameConfiguration.LevelScreen;
import gameStructure.BaseActor;

/**
 * Clase publica que dibuja el orbe que dispara el fantasma. Se declaran
 * parametros como el tiempo que va a estar el orbe en pantalla, la velocidad a
 * la que se dispara el orbe, la direccion del orbe, y un contador-cronometro
 * que contabilizara el tiempo que permanece en pantalla el mismo.
 * 
 * @author Iulian Preda
 *
 */
public class BulletBomb extends BaseActor {
	protected LevelScreen level;
	protected float bulletTimer;
	protected float timer;
	protected float bulletSpeed;
	protected int bulletDirection;
	protected boolean enabled;

	public BulletBomb(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s);

		loadTexture("assets/atk/1.png");
		bulletTimer = 10;
		this.level = nivel;
		bulletSpeed = 600;
		enabled = false;
	}

	/**
	 * Metodo que verifica si el orbe-proyectil se encuentra habilitado. Si se
	 * encuentra habilitado, se dibujara en pantalla. Si no se encuentra habilidado,
	 * no aparecera.
	 */
	@SuppressWarnings("static-access")
	@Override
	public void act(float dt) {

		if (enabled) {
			super.act(dt);
			this.rotateBy(-10 * bulletDirection);
			moveBy(velocityVec.x * dt, velocityVec.y * dt);

			if (this.enabled && this.overlaps(level.jugador)) {
				level.jugador.dano(10);
				this.enabled = false;

			}
			for (Solid solido : level.getSolidos()) {
				if (this.overlaps(solido)) {
					this.enabled = false;
				}

			}
			timer -= dt;

			if (timer <= 0) {
				enabled = false;
			}

		}
	}

	/**
	 * Metodo para dibujar el proyectil en pantalla. Solo se vera el proyectil si
	 * esta activado
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {

		if (enabled)
			super.draw(batch, parentAlpha);
	}

	/**
	 * Metodo que genera el proyectil que aparecera en pantalla. En este metodo se
	 * encuentran parametros como la posicion en pantalla y el tiempo que estara en
	 * pantalla. Tambien hay una variable interruptor.
	 * 
	 * @param x
	 * @param y
	 * @param posx
	 * @param posy
	 */
	public void generarOrbe(float x, float y, float posx, float posy) {

		this.enabled = true;

		this.setPosition(posx, posy);

		timer = bulletTimer;
		this.velocityVec.set(bulletSpeed * x, bulletSpeed * y);
		if (posx < 0) {
			bulletDirection = -1;
		} else {
			bulletDirection = 1;
		}
	}

}

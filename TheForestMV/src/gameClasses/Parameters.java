package gameClasses;

/**
 * Clase publica que establece parametros que se utilizan a lo largo del juego.
 * 
 * @author Iulian Preda.
 *
 */
public class Parameters {

	public static float enemyGravity = 1050;
	private static float gravity = 650;
	public static int width = 1500;
	public static int height = 600;
	public static int level = 1;
	public static boolean end = true;
	public static boolean victory = true;

	public static float getGravedad() {
		return gravity;
	}

	public static void setGravedad(float gravedad) {
		Parameters.gravity = gravedad;
	}

	public static float getGravedadEnemigos() {
		return enemyGravity;
	}

	public static void setGravedadEnemigos(float gravedadEnemigos) {
		Parameters.enemyGravity = gravedadEnemigos;
	}

}

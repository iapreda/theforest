package gameClasses;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import gameStructure.BaseActor;

/**
 * Clase para agregar un objeto al juego.
 * @author Iulian Preda.
 *
 */

public class FastBulletPerk extends BaseActor {

	public FastBulletPerk(float x, float y, Stage s) {
		super(x, y, s);
		loadTexture("assets/items/gun.png");

		rotateBy(10);

		Action tilt = Actions.sequence(Actions.rotateBy(-20, 0.2f), Actions.rotateBy(20, 0.5f));

		addAction(Actions.forever(tilt));
	}
}

package gameClasses;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameConfiguration.LevelScreen;
import gameStructure.BaseActor;

/**
 * Clase que crea las balas que va a disparar el enemigo.
 * 
 * @author Iulian Preda.
 *
 */
public class EnemyBullet extends BaseActor {

	protected LevelScreen level;
	protected float bulletTimer;
	protected float timeTimer;
	protected float bulletSpeed;
	protected int direction;
	protected boolean enabled;

	/**
	 * Constructor de la clase. Inicializo variables.
	 * 
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public EnemyBullet(float x, float y, Stage s, LevelScreen nivel) {

		super(x, y, s);
		loadTexture("assets/atk/1.png");
		bulletTimer = 10;
		level = nivel;
		bulletSpeed = 800;
		enabled = false;
	}

	/**
	 * Metodo para actualizar el funcionamiento del proyectil enemigo.
	 */
	@SuppressWarnings("static-access")
	@Override
	public void act(float dt) {
		// TODO Auto-generated method stub

		if (enabled) {
			super.act(dt);
			this.rotateBy(-10 * direction);
			moveBy(velocityVec.x * dt, velocityVec.y * dt);

			if (this.enabled && this.overlaps(level.jugador)) {
				level.jugador.dano(10);
				this.enabled = false;

			}
			for (Solid solido : level.getSolidos()) {// miramos si el proyectil ha impactado en algun solido
				if (this.overlaps(solido)) {
					this.enabled = false; // desactivamos la bala tras el impacto
				}

			}
			timeTimer -= dt;

			if (timeTimer <= 0) {
				enabled = false;
			}

		}
	}

	/**
	 * Metodo para dibujar el proyectil del enemigo en pantalla.
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		if (enabled)
			super.draw(batch, parentAlpha);
	}

	/**
	 * Metodo para hacer que el enemigo pueda disparar.
	 * 
	 * @param x
	 * @param y
	 * @param posx
	 * @param posy
	 */
	public void disparar(float x, float y, float posx, float posy) {

		this.enabled = true;

		this.setPosition(posx, posy);

		timeTimer = bulletTimer;
		this.velocityVec.set(bulletSpeed * x, bulletSpeed * y);
		if (posx < 0) {
			direction = -1;
		} else {
			direction = 1;
		}
	}

}

package gameClasses;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameStructure.BaseActor;

/**
 * Metodo que dibuja un portal en el mapa. Tambien agrega una animacion al
 * mismo.
 * 
 * @author Iulian Preda
 *
 */
public class NextPortal extends BaseActor {

	@SuppressWarnings("rawtypes")
	Animation portal;

	public NextPortal(float x, float y, Stage s) {
		super(x, y, s);

		String[] dimension = { "assets/portal/1.png", "assets/portal/2.png", "assets/portal/3.png",
				"assets/portal/4.png" };
		portal = loadAnimationFromFiles(dimension, 0.1f, true);
	}

}

package gameClasses;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameConfiguration.LevelScreen;
import gameStructure.BaseActor;

/**
 * Clase para dibujar un enemigo en pantalla.
 * 
 * @author iulia
 *
 */
public class ChaseSlider extends Enemy {

	
	@SuppressWarnings("rawtypes")
	public Animation walk;
	public BaseActor lowerHitbox;
	public BaseActor higherHitbox;

	boolean chase;

	public float maxSpeed;
	public float acceleration;

	public int aggro;
	public int direction;
	
	

	/**
	 * Constructor de la clase. Inicio variables ya agrego una animacion al enemigo.
	 * 
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public ChaseSlider(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s, nivel);
		// TODO Auto-generated constructor stub

		String[] walkFileNames = { "assets/deslizador/1.png", "assets/deslizador/2.png", "assets/deslizador/3.png",
				"assets/deslizador/4.png", "assets/deslizador/5.png", "assets/deslizador/6.png",
				"assets/deslizador/7.png", "assets/deslizador/8.png", "assets/deslizador/9.png",
				"assets/deslizador/10.png", "assets/deslizador/11.png", "assets/deslizador/12.png",
				"assets/deslizador/13.png" };
		walk = loadAnimationFromFiles(walkFileNames, 0.1f, true);
		maxSpeed = 100;
		acceleration = 900;
		direction = 1;
		chase = false;
		aggro = 150;

		lowerHitbox = new BaseActor(0, 0, s);
		lowerHitbox.setSize(this.getWidth() / 6, 30);
		lowerHitbox.setBoundaryRectangle();
		adjustLowerHitbox();

		higherHitbox = new BaseActor(0, 0, s);
		higherHitbox.setSize(this.getWidth() / 6, 30);
		higherHitbox.setBoundaryRectangle();

		adjustHigherHitbox();

	}

	/**
	 * Metodo para actualizar el enemigo.
	 */
	@SuppressWarnings("static-access")
	@Override
	public void act(float dt) {
		super.act(dt);
		boolean floor = false;
		boolean head = false;
		if (Math.sqrt(Math.pow(this.getX() - level.jugador.getX(), 2)
				+ Math.pow(this.getY() - level.jugador.getY(), 2)) < aggro) {
			if (level.jugador.getX() < this.getX()) {
				direction = -1;
			} else {
				direction = 1;
			}
			if (direction > 0) {
				setScaleX(-1);
			} else {
				setScaleX(1);
			}
			moveBy(direction * maxSpeed * dt, velocityVec.y * dt);
		} else {
			for (Solid solido : level.getSolidos()) {
				if (lowerHitbox.overlaps(solido)) {
					floor = true;
				}
				if (higherHitbox.overlaps(solido)) {
					head = true;
				}

			}

			if (head || !floor) {
				direction *= -1;
			}
			if (direction > 0) {
				setScaleX(-1);
			} else {
				setScaleX(1);
			}
			moveBy(direction * maxSpeed * dt, velocityVec.y * dt);
		}

		adjustLowerHitbox();
		adjustHigherHitbox();

		boundToWorld();

	}

	/**
	 * Metodo para ajustar la hitbox superior.
	 */
	private void adjustHigherHitbox() {
		if (direction == -1) {
			higherHitbox.setPosition(this.getX() - 15, this.getY() + 10);
		} else {
			higherHitbox.setPosition(this.getX() + this.getWidth() + 15 - this.getWidth() / 6, this.getY() + 10);
		}

	}

	/**
	 * Metodo para ajustar la hitbox inferior.
	 */
	private void adjustLowerHitbox() {
		if (direction == -1) {
			lowerHitbox.setPosition(this.getX() + 15, this.getY() - 20);
		} else {
			lowerHitbox.setPosition(this.getX() + this.getWidth() - 15 - this.getWidth() / 6, this.getY() - 20);
		}

	}
	
//	private void perseguir(float x, float y, boolean accurate) {
//		float margen = 0;
//		if (!accurate) {
//			margen = 400;
//		}
//
//		if (x + margen < this.getX()) {
//			accelerationVec.add(-acceleration, 0);
//
//		} else if (x - margen > this.getX()) {
//			accelerationVec.add(acceleration, 0);
//		}
//		if (x + margen < this.getX()) {
//			accelerationVec.add(0, -acceleration);
//
//		} else if (x - margen > this.getX()) {
//			accelerationVec.add(0, acceleration);
//		}
//	}

}

package gameClasses;

import java.io.File;
import java.util.Scanner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import gameConfiguration.BaseScreen;
import gameConfiguration.Lose;
import gameConfiguration.TitleElements;
import gameStructure.BaseGame;

/**
 * Clase para agregar una pantalla de 'Game Over' cuando el jugador se queda sin
 * vida.
 * 
 * @author Iulian Preda.
 *
 */

public class GameOverScreen extends BaseScreen {

	public Label msg;
	public Music theme;
	public float volume;

	/**
	 * Este metodo comprueba que el volumen este activado o no. A su vez, si el
	 * jugador se queda sin vida, aparecerá una pantalla de 'game over'.
	 */
	@SuppressWarnings("unused")
	@Override
	public void initialize() {

		String line = null;
		Scanner scan = null;
		File file = new File("Configuracion");
		theme = Gdx.audio.newMusic(Gdx.files.internal("assets/sound/gover.wav"));

		try {
			scan = new Scanner(file);
			line = scan.nextLine();

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			try {
				if (scan != null)
					scan.close();
			} catch (Exception ex) {
				System.out.println("Mensaje 2: " + ex.getMessage());
			}
		}

		if (line.equals("1")) {
			volume = 0.2f;
		} else {
			volume = 0;
		}

		theme.setLooping(false);
		theme.setVolume(volume);
		theme.play();// Inicio los sonidos

		// TODO Auto-generated method stub

		msg = new Label("", BaseGame.labelStyle);
		if (Parameters.end = true) {
			Lose lose = new Lose(0, 0, backStage, this);
		}

		uiStage.addActor(msg);
		msg.setPosition(200, 200);
	}

	/**
	 * Metodo para actualizar la clase. Si el jugador pula 'ESC' mientras esta en la
	 * pantalla de game over, le llevara al menu.
	 */
	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			this.dispose();

			BaseGame.setActiveScreen(new TitleElements());
		}

	}

}

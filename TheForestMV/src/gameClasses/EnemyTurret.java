package gameClasses;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameConfiguration.LevelScreen;


/**
 * Clase publica que hereda de Enemigo.
 * 
 * En esta clase se declaran las variables necesarias para que el enemigo
 * funcione de manera optima.
 * @author Iulian Preda
 *
 */

public class EnemyTurret extends Enemy {

	@SuppressWarnings("rawtypes")
	Animation atk;
	@SuppressWarnings("rawtypes")
	Animation def;

	public ArrayList<EnemyBullet> bullets;
	public float shotTimer;
	public float shotCount;
	public float startX;
	public float startY;
	
	public int bulletNumber;
	public int actualBullet;
	public int aggro = 350;

	public boolean cumple;
	
	/**
	 * Constructor de la clase donde se incializan las variables. En el constructor
	 * tambien se crea una animacion para que el enemigo tenga movimiento fluido.
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public EnemyTurret(float x, float y, Stage s, LevelScreen nivel) {

		super(x, y, s, nivel);

		EnemyBullet bullet;
		String[] atkFiles = { "assets/turretatk/atk00.png", "assets/turretatk/atk01.png",
				"assets/turretatk/atk02.png", "assets/turretatk/atk03.png", "assets/turretatk/atk04.png"};
		atk = loadAnimationFromFiles(atkFiles, 0.3f, true);
		
		String[] defFiles = {"assets/turretidle/def00.png", "assets/turretidle/def01.png",
				"assets/turretidle/def02.png", "assets/turretidle/def03.png", "assets/turretidle/def04.png"};
		def = loadAnimationFromFiles(defFiles, 0.3f, true);
		
		bulletNumber = 5;
		actualBullet = 0;
		startX = x;
		startY = y;
		bullets = new ArrayList<EnemyBullet>();
		
		shotTimer = 1;
		shotCount = 0;
		for (int i = 0; i < bulletNumber; i++) {
			bullet = new EnemyBullet(0, 0, s, nivel);
			bullets.add(bullet);
		}
	}

	/**
	 * Metodo que verifica si el enemigo se encuentra habilitado. Si se encuentra
	 * habilitado, se dibujara en pantalla. Si no se encuentra habilidado, no
	 * aparecera.
	 * 
	 * En este metodo tambien se encuentra un IF que controla la colision con el
	 * terreno.
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	@Override
	public void act(float dt) {
		// TODO Auto-generated method stub
		super.act(dt);
		if (this.isAlive) {
			setAnimation(def);
			if (Math.sqrt(Math.pow(this.getX() - level.jugador.getX(), 2)
					+ Math.pow(this.getY() - level.jugador.getY(), 2)) < aggro) {
				setAnimation(atk);
				if (this.shotCount <= 0) {
					shot();
				} else {
					shotCount -= dt;
				}
			}

		}
	}

	/**
	 * Metodo para hacer que el enemigo dispare.
	 */
	@SuppressWarnings("static-access")
	private void shot() {
		this.shotCount = this.shotTimer;

		float x = this.getX() - level.jugador.getX() - 1;
		float y = this.getY() - level.jugador.getY() - 1;
		Vector2 vector = new Vector2(x, y).nor();
		bullets.get(actualBullet).disparar(vector.x * -1, vector.y * -1, this.getX(), this.getY()+50);
		actualBullet++;
		actualBullet = actualBullet % bulletNumber;

	}

}

package gameClasses;

import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Metodo para asignar una textura a un objeto.
 * 
 * @author Iulian Preda.
 *
 */

public class Platform extends Solid {
	public Platform(float x, float y, Stage s) {
		super(x, y, 32, 16, s);
		loadTexture("assets/individualitems/1.png");
	}
}
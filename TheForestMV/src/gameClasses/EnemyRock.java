package gameClasses;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameConfiguration.LevelScreen;
import gameStructure.BaseActor;

/**
 * Clase para crear uno de los enemigos que se dibujan en pantalla.
 * 
 * @author Iulian Preda.
 *
 */
public class EnemyRock extends Enemy {

	public BaseActor higherHitbox;
	public BaseActor lowerHitbox;
	@SuppressWarnings("rawtypes")
	public Animation idle;
	@SuppressWarnings("rawtypes")
	public Animation jumping;

	public float maxSpeed;
	public float acceleration;
	public float jumpSpeed;
	public float idleTimer;
	public float stillTimer;

	/**
	 * Constructor de la clase. Inicializo variables.
	 * 
	 * @param x
	 * @param y
	 * @param s
	 * @param nivel
	 */
	public EnemyRock(float x, float y, Stage s, LevelScreen nivel) {

		super(x, y, s, nivel);

		String[] jumpFiles = { "assets/jump/r.png" };
		jumping = loadAnimationFromFiles(jumpFiles, 0.1f, true);

		idle = loadTexture("assets/jump/r.png");
		maxSpeed = 200;
		acceleration = 400;
		idleTimer = 1;
		jumpSpeed = 350;

		higherHitbox = new BaseActor(0, 0, s);
		higherHitbox.setSize(this.getWidth() * 5 / 6, 15);
		higherHitbox.setBoundaryRectangle();

		lowerHitbox = new BaseActor(0, 0, s);
		lowerHitbox.setSize(this.getWidth() / 6, 30);
		lowerHitbox.setBoundaryRectangle();

	}

	/**
	 * Constructor para actualizar la clase.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void act(float dt) {
		super.act(dt);

		accelerationVec.add(0, -Parameters.getGravedadEnemigos());
		if (this.isOnSolid()) {
			if (stillTimer <= 0) {
				this.jump();
				this.stillTimer = idleTimer;
			} else {
				this.setAnimation(idle);
				this.velocityVec.set(0, 0);
				accelerationVec.set(0, 0);
				stillTimer -= dt;
			}
		}

		velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
		moveBy(velocityVec.x * dt, velocityVec.y * dt);
		accelerationVec.set(0, 0);

		adjustLowerHitbox();
		adjustHigherHitbox();
		boundToWorld();

	}

	/**
	 * Metodo para comprobar si el enemigo est� en terreno solido o no.
	 * 
	 * @return
	 */
	public boolean isOnSolid() {
		for (Solid solido : level.getSolidos()) {
			if (this.higherHitbox.overlaps(solido) && solido.isEnabled()) {
				return true;
			} else if (this.lowerHitbox.overlaps(solido)) {
				return true;
			}

		}

		return false;

	}

	/**
	 * Metodo para hacer que el enemigo salte.
	 */
	public void jump() {
		velocityVec.y = jumpSpeed * 1.2f;

	}

	/**
	 * Metodo para ajustar la hitbox del enemigo.
	 */
	private void adjustHigherHitbox() {
		lowerHitbox.setPosition(this.getX() - 15, this.getY() + 10);
	}

	/**
	 * Metodo para ajustar la hitbox del enemigo.
	 */
	private void adjustLowerHitbox() {
		higherHitbox.setPosition(this.getX() + this.getWidth() / 3, this.getY() - 5);

	}
}

package gameConfiguration;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import gameClasses.Parameters;
import gameStructure.BaseGame;


/**
 * Clase para crear un menu de configuracion.
 * @author Iulian Preda.
 *
 */
public class Configuration extends BaseScreen {

	private Table table;
	private Label labelMsg;

	private Music theme;
	private float volume;

	/**
	 * Este metodo disena la pantalla para poder ver los elementos.
	 */
	@SuppressWarnings("unused")
	@Override
	public void initialize() {

		theme = Gdx.audio.newMusic(Gdx.files.internal("assets/music/1.mp3"));

		((OrthographicCamera) this.mainStage.getCamera()).setToOrtho(false, Parameters.width / 3,
				Parameters.height / 3);
		Background fondoInicio = new Background(0, 0, backStage, null);

		labelMsg = new Label("SONIDO", BaseGame.labelStyle);
		uiStage.addActor(labelMsg);
		labelMsg.setPosition(790, 600);

		table = new Table();
		table.setFillParent(true);
		this.uiStage.addActor(table);

		TextButton onButton = new TextButton("ON", BaseGame.textButtonStyle);
																			
		onButton.addListener((Event e) -> {
			if (!(e instanceof InputEvent) || !((InputEvent) e).getType().equals(Type.touchDown))
				return false;
			this.dispose();
			sonidoOn();
			return false;
		});
		table.add(onButton);

		TextButton offButton = new TextButton("OFF", BaseGame.textButtonStyle);// Boton de jugar, al presionar emopiezas a
																				// juagar
		offButton.addListener((Event e) -> {
			if (!(e instanceof InputEvent) || !((InputEvent) e).getType().equals(Type.touchDown))
				return false;
			this.dispose();
			sonidoOff();
			return false;
		});
		table.add(offButton);

		TextButton backButton = new TextButton("Volver", BaseGame.textButtonStyle);// Boton de jugar, al presionar
																					// emopiezas a juagar
		backButton.addListener((Event e) -> {
			if (!(e instanceof InputEvent) || !((InputEvent) e).getType().equals(Type.touchDown))
				return false;
			this.theme.dispose();// Desactiva la musica
			this.dispose();
			BaseGame.setActiveScreen(new TitleElements());
			return false;
		});
		table.add(backButton);

	}

	/**
	 * Metodo para actualizar la clase.
	 */
	@Override
	public void update(float dt) {

		String line = null;
		Scanner scan = null;
		File file = new File("Configuracion");

		try {
			scan = new Scanner(file);
			line = scan.nextLine();

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			try {
				if (scan != null)
					scan.close();
			} catch (Exception ex) {
				System.out.println("Mensaje 2: " + ex.getMessage());
			}
		}

		if (line.equals("1")) {
			volume = 0.2f;
		} else {
			volume = 0;
		}

		theme.setLooping(true);
		theme.setVolume(volume);
		theme.play();// Inicio los sonidos
	}

	/**
	 * Metodo para actualizar el sonido. Escribe '1' en un fichero. 
	 * 1 significa que el sonido se activa.
	 */
	public void sonidoOn() {
		String line = "1";

		
		FileWriter file = null;
		try {

			file = new FileWriter("Configuracion");
			file.write(line);
			
			file.close();

		} catch (Exception ex) {
			System.out.println("Mensaje de la excepci�n: " + ex.getMessage());
		}
	}

	/**
	 * Metodo para actualizar el sonido. Escribe '0' en un fichero. 
	 * 0 significa que el sonido se desactiva.
	 */
	public void sonidoOff() {
		String line = "0";

		FileWriter file = null;
		try {

			file = new FileWriter("Configuracion");

			file.write(line);

			file.close();

		} catch (Exception ex) {
			System.out.println("Mensaje de la excepcion: " + ex.getMessage());
		}
	}

}

package gameConfiguration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import gameStructure.BaseGame;

/**
 * Clase que se utiliza para la creacion del menu y los elemenos del mismo.
 * 
 * @author Yuliann
 *
 */
public class TitleElements extends BaseScreen {
	public Table table;

	public boolean enabledBoots;
	public boolean musica;
	public boolean start;
	public boolean enabledGravity;
	public boolean enabledPerk;
	public ArrayList<Object> elements;
	LevelScreen levelScreen;

	public TitleElements() {
		this.enabledBoots = false;
		this.enabledGravity = false;
		this.enabledPerk = false;
		this.start = true;
		elements = new ArrayList<Object>();
		uiTable = new Table();

		String line = null;

		File file = new File("Configuracion");

		if (file.isFile()) {
			Scanner s = null;

			try {
				s = new Scanner(file);
				line = s.nextLine();

			} catch (Exception ex) {
				System.out.println("Mensaje: " + ex.getMessage());
			} finally {

				try {
					if (s != null)
						s.close();
				} catch (Exception ex2) {
					System.out.println("Mensaje 2: " + ex2.getMessage());
				}

			}
		} else {
			String newLine = "1";

			FileWriter fw = null;
			try {

				fw = new FileWriter("Configuracion");

				fw.write(newLine);

				fw.close();

			} catch (Exception ex) {
				System.out.println("Mensaje de la excepci�n: " + ex.getMessage());
			}
		}

	}

	/**
	 * Metodo que inicializa los elementos.
	 */
	@SuppressWarnings("unused")
	@Override
	public void initialize() {
		table = new Table();
		table.setFillParent(true);
		uiStage.addActor(table);

		Background background = new Background(0, 0, backStage, levelScreen);
		Forest forest = new Forest(247, 360, backStage, null);
		table.row();
		table.add().left();
		table.row();
		TextButton btnPlay = new TextButton("Play", BaseGame.textButtonStyle);

		btnPlay.addListener((Event e) -> {
			if (!(e instanceof InputEvent) || !((InputEvent) e).getType().equals(Type.touchDown))
				return false;
			this.dispose();
			BaseGame.setActiveScreen(
					new LevelScreen(enabledBoots, enabledGravity, enabledPerk, elements, start, musica));
			return false;
		});
		table.add(btnPlay);

		TextButton btnOptions = new TextButton("Opciones", BaseGame.textButtonStyle);// Boton de jugar, al presionar
																						// emopiezas a juagar
		btnOptions.addListener((Event e) -> {
			if (!(e instanceof InputEvent) || !((InputEvent) e).getType().equals(Type.touchDown))
				return false;
			BaseGame.setActiveScreen(new Configuration());
			return false;
		});
		table.add(btnOptions);

		TextButton btnSalir = new TextButton("Salir", BaseGame.textButtonStyle);// Boton para cerrar el juego
		btnSalir.addListener((Event e) -> {
			if (!(e instanceof InputEvent) || !((InputEvent) e).getType().equals(Type.touchDown))
				return false;
			this.dispose();
			Gdx.app.exit();
			return false;
		});
		table.add(btnSalir);

	}

	@Override
	public void update(float dt) {

	}

}

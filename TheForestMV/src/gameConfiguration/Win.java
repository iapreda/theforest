package gameConfiguration;

import com.badlogic.gdx.scenes.scene2d.Stage;

import gameClasses.GameWinScreen;
import gameStructure.BaseActor;

/**
 * Clase que agrega una imagen de 'win'
 * @author Iulian Preda.
 *
 */

public class Win extends BaseActor {

	public Win(float x, float y, Stage s, GameWinScreen gameWinScreen) {
		super(x, y, s);
		loadTexture("menu/win.jpg");
		
	}

	

}
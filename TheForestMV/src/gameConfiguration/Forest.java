package gameConfiguration;

import com.badlogic.gdx.scenes.scene2d.Stage;

import gameStructure.BaseActor;

/**
 * Clase para agregar una imagen a la clase 'Title Elements'
 * 
 * @author Iulian Preda.
 *
 */
public class Forest extends BaseActor {

	public Forest(float x, float y, Stage s, LevelScreen pantallainicio) {
		super(x, y, s);
		loadTexture("menu/title.png");

	}

}
package gameConfiguration;

import com.badlogic.gdx.scenes.scene2d.Stage;

import gameClasses.GameOverScreen;
import gameStructure.BaseActor;

/**
 * Clase que agrega una imagen de 'game over'
 * @author Iulian Preda.
 *
 */

public class Lose extends BaseActor {

	public Lose(float x, float y, Stage s, GameOverScreen gameOverScreen) {
		super(x, y, s);
		loadTexture("menu/lose.jpg");
		
	}

	

}
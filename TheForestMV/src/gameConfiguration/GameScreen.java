package gameConfiguration;

import gameStructure.BaseGame;

/**
 * Clase que da paso al titulo.
 * @author Iulian Preda.
 *
 */
public class GameScreen extends BaseGame{

	public void create() {
		super.create();
		
		setActiveScreen(new TitleElements());
		
	}
	
}

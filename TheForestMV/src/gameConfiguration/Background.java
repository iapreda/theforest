package gameConfiguration;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;

import gameClasses.Parameters;
import gameStructure.BaseActor;

/**
 * Clase publica que se utiliza para poner un fondo a los niveles que sean
 * necesarios. Se puede utilizar para poner un fondo diferente para cada nivel.
 * Hereda de la clase BaseActor.
 * 
 * @author Iulian Preda.
 * @see BaseActor
 *
 */
public class Background extends BaseActor {
	OrthographicCamera camera, cameraLevel;
	Stage s;
	LevelScreen level;

	public Background(float x, float y, Stage s, LevelScreen nivel) {
		super(x, y, s);
		// TODO Auto-generated constructor stub
		if (Parameters.level == 1) {
			loadTexture("assets/maps/1.jpg");
		} else if (Parameters.level == 2) {
			loadTexture("assets/maps/1.jpg");
		} else if (Parameters.level == 3) {
			loadTexture("assets/maps/1.jpg");
		} else {
			loadTexture("assets/maps/1.jpg");
		}
		this.s = s;
		this.level = nivel;

	}

	/**
	 * Metodo que actualiza la clase.
	 */
	@Override
	public void act(float dt) {

		super.act(dt);
	}

}

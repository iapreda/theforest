package gameConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import character.Character;
import gameClasses.BootsPerk;
import gameClasses.EndPortal;
import gameClasses.Enemy;
import gameClasses.EnemyFliyingBomb;
import gameClasses.EnemyRock;
import gameClasses.EnemySlider;
import gameClasses.EnemyTurret;
import gameClasses.FastBulletPerk;
import gameClasses.GameOverScreen;
import gameClasses.GameWinScreen;
import gameClasses.GravityPerk;
import gameClasses.NextPortal;
import gameClasses.Parameters;
import gameClasses.Platform;
import gameClasses.ReturnPortal;
import gameClasses.ChaseSlider;
import gameClasses.Solid;
import gameClasses.Terrain;
import gameStructure.BaseActor;
import gameStructure.BaseGame;
import main.Launcher;

/**
 * Clase publica donde ocurre la magia del juego. En esta clase est� el cuerpo.
 * 
 * @author Iulian Preda.
 *
 */
public class LevelScreen extends BaseScreen {

	static public TilemapActor tma;
	static public Character jugador;
	static public MapObject savePoint;
	static public MapProperties startProps;
	static public MapProperties lastProps;
	static public MapObject startPoint;
	static public MapObject lastPoint;

	public BaseActor doubleJump;
	public BaseActor noGravity;
	public BaseActor speedShot;
	public ArrayList<Solid> solid;
	public ArrayList<Enemy> enemy;
	public ArrayList<NextPortal> nextPortal;
	public ArrayList<EndPortal> end;
	public ArrayList<GravityPerk> gravityPerk;
	public ArrayList<FastBulletPerk> fbPerk;
	public ArrayList<ReturnPortal> returnPortal;
	public ArrayList<Platform> plataforms;
	public ArrayList<BootsPerk> doubleJumpPerk;
	public ArrayList<Object> elements;
	public ArrayList<EnemyFliyingBomb> fly;
	public ArrayList<EnemyRock> rock;
	public ArrayList<ChaseSlider> slider;
	public ArrayList<EnemyTurret> turret;
	public ArrayList<Character> pnj;

	public Table iconBoot;
	public Table iconZeroG;
	public Table iconGun;
	public Label vidaRestante;

	public boolean enabledBoots = false;
	public boolean start = true;
	public boolean enabledGravity = false;
	public boolean enabledPerk = false;
	public boolean nextLvl = false;
	public boolean lastLvl = false;
	public boolean music = false;
	public boolean active = false;
	public boolean activePerk = false;
	public boolean respawnPoint = false;
	public boolean gameOver;

	public int timer;
	public int timerPerk;
	public int timerCooldown;
	public int bootIcon;
	public int gravityIcon;
	public int perkIcon;

	public Music theme;
	public Sound jump;
	public Sound shot;

	public int maxJump;
	public float volumen;

	public LevelScreen(boolean enabledBoots, boolean enabledGravity, boolean enabledPerk, ArrayList<Object> elements,
			boolean start, boolean musica) {
		this.enabledBoots = enabledBoots;
		this.music = musica;
		this.enabledGravity = enabledGravity;
		this.enabledPerk = enabledPerk;
		this.elements = elements;
		this.start = start;

		if (start) {
			start();
		} else {
			last();
		}

	}

	private void last() {
		jugador = new Character((float) lastProps.get("x"), (float) lastProps.get("y"), mainStage, this);

	}

	private void start() {
		jugador = new Character((float) startProps.get("x"), (float) startProps.get("y"), mainStage, this);

	}

	/**
	 * Metodo para inicializar todas las variables y arrays utilizados en el juego.
	 */
	@SuppressWarnings("unused")
	@Override
	public void initialize() {

		Background fondo = new Background(0, 0, backStage, this);

		/**
		 * Switch case que establece el orden de los mapas.
		 */

		switch (Parameters.level) {
		case 1:
			tma = new TilemapActor("assets/maps/map1.tmx", mainStage);
			break;
		case 2:
			tma = new TilemapActor("assets/maps/map2.tmx", mainStage);
			break;
		case 3:
			tma = new TilemapActor("assets/maps/map3.tmx", mainStage);
			break;
		case 4:
			tma = new TilemapActor("assets/maps/map4.tmx", mainStage);
			break;
		case 5:
			tma = new TilemapActor("assets/maps/map5.tmx", mainStage);
			break;
		}

		startPoint = tma.getTileList("start").get(0);
		startProps = startPoint.getProperties();

		lastPoint = tma.getTileList("lastPoint").get(0);
		lastProps = lastPoint.getProperties();

		Terrain suelo;
		MapProperties props;

		/*
		 * Arrays inicializados.
		 */

		pnj = new ArrayList<Character>();
		solid = new ArrayList<Solid>();
		doubleJumpPerk = new ArrayList<BootsPerk>();
		gravityPerk = new ArrayList<GravityPerk>();
		enemy = new ArrayList<Enemy>();
		nextPortal = new ArrayList<NextPortal>();
		end = new ArrayList<EndPortal>();
		turret = new ArrayList<EnemyTurret>();
		slider = new ArrayList<ChaseSlider>();
		fly = new ArrayList<EnemyFliyingBomb>();
		rock = new ArrayList<EnemyRock>();
		fbPerk = new ArrayList<FastBulletPerk>();
		returnPortal = new ArrayList<ReturnPortal>();
		plataforms = new ArrayList<Platform>();
		elements = new ArrayList<Object>();

		iconBoot = new Table();
		iconZeroG = new Table();
		iconGun = new Table();

		timer = 120;
		timerCooldown = 800;
		timerPerk = 55;
		bootIcon = 0;
		gravityIcon = 0;
		perkIcon = 0;
		maxJump = 5;

		for (MapObject obj : tma.getRectangleList("terreno")) {
			props = obj.getProperties();
			suelo = new Terrain((float) props.get("x"), (float) props.get("y"), (float) props.get("width"),
					(float) props.get("height"), mainStage);
			solid.add(suelo);
		}

		BootsPerk bota;
		for (MapObject obj : tma.getTileList("boots")) {
			props = obj.getProperties();
			bota = new BootsPerk((float) props.get("x"), (float) props.get("y"), mainStage);
			doubleJumpPerk.add(bota);
		}

		GravityPerk zeroGravity;
		for (MapObject obj : tma.getTileList("gravity")) {
			props = obj.getProperties();
			zeroGravity = new GravityPerk((float) props.get("x"), (float) props.get("y"), mainStage);
			gravityPerk.add(zeroGravity);
		}
		FastBulletPerk fbp;
		for (MapObject obj : tma.getTileList("fast")) {
			props = obj.getProperties();
			fbp = new FastBulletPerk((float) props.get("x"), (float) props.get("y"), mainStage);
			fbPerk.add(fbp);
		}

		NextPortal portalnext;
		for (MapObject obj : tma.getRectangleList("portal")) {
			props = obj.getProperties();
			portalnext = new NextPortal((float) props.get("x"), (float) props.get("y"), mainStage);
			nextPortal.add(portalnext);
		}

		EndPortal portalFin;
		for (MapObject obj : tma.getRectangleList("fin")) {
			props = obj.getProperties();
			portalFin = new EndPortal((float) props.get("x"), (float) props.get("y"), mainStage);
			end.add(portalFin);
		}

		ReturnPortal retPortal;
		for (MapObject obj : tma.getRectangleList("last")) {
			props = obj.getProperties();
			retPortal = new ReturnPortal((float) props.get("x"), (float) props.get("y"), mainStage);
			returnPortal.add(retPortal);

		}

		Platform platforms;
		for (MapObject obj : tma.getTileList("plataforma")) {

			props = obj.getProperties();
			platforms = new Platform((float) props.get("x"), (float) props.get("y"), mainStage);
			plataforms.add(platforms);

		}

		Enemy enemigo;
		for (MapObject obj : tma.getTileList("torreta")) {

			props = obj.getProperties();
			enemigo = new EnemyTurret((float) props.get("x"), (float) props.get("y"), mainStage, this);
			enemy.add(enemigo);
		}

		for (MapObject obj : tma.getTileList("avion")) {

			props = obj.getProperties();
			enemigo = new EnemyFliyingBomb((float) props.get("x"), (float) props.get("y"), mainStage, this);
			enemy.add(enemigo);
		}

		for (MapObject obj : tma.getTileList("slider")) {
			props = obj.getProperties();
			enemigo = new ChaseSlider((float) props.get("x"), (float) props.get("y"), mainStage, this);
			enemy.add(enemigo);
		}

		for (MapObject obj : tma.getTileList("roca")) {
			props = obj.getProperties();
			enemigo = new EnemyRock((float) props.get("x"), (float) props.get("y"), mainStage, this);
			enemy.add(enemigo);
		}

		for (MapObject obj : tma.getRectangleList("crab")) {
			props = obj.getProperties();
			enemigo = new EnemySlider((float) props.get("x"), (float) props.get("y"), mainStage, this);
			enemy.add(enemigo);
		}

		/*
		 * Variables para agregarle sonido al juego.
		 */
		theme = Gdx.audio.newMusic(Gdx.files.internal("assets/music/1.mp3"));
		jump = Gdx.audio.newSound(Gdx.files.internal("assets/sound/1.mp3"));
		shot = Gdx.audio.newSound(Gdx.files.internal("assets/sound/2.mp3"));

		volumen = 0.2f;

		String line = null;
		File file = new File("Configuracion");
		Scanner scan = null;

		try {
			scan = new Scanner(file);
			line = scan.nextLine();

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {

			try {
				if (scan != null)
					scan.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}

		if (line.equals("1")) {
			volumen = 0.2f;
		} else {
			volumen = 0;
		}

		switch (Parameters.level) {
		case 1:
			music = false;
			this.theme.setLooping(true);
			this.theme.setVolume(volumen);
			theme.play();
			break;
		}

		this.vidaRestante = new Label("Vida: ", BaseGame.labelStyle);
		this.uiTable.add(vidaRestante).left().expandX();
		this.uiTable.add().left();
		this.uiTable.add(iconBoot);
		this.uiTable.add(iconZeroG);
		this.uiTable.add(iconGun);
		this.uiTable.row();
		this.uiTable.add().expandY();

	}

	/**
	 * Metodo que actualiza el estado de los elementos en pantalla.
	 */
	@Override
	public void update(float dt) {

		for (BaseActor actor : BaseActor.getList(mainStage, "gameClasses.Solid")) {
			Solid solid = (Solid) actor;

			if (solid instanceof Platform) {
				// Deshabilita el solido cuando salta por debajo
				if (jugador.isJumping() && jugador.overlaps(solid)) {
					solid.setEnabled(false);
				}
				// habilita el solid cuando lo ha atravesado
				if (jugador.isJumping() && !jugador.overlaps(solid)) {
					solid.setEnabled(true);
				}
				// deshabilita el solido cunado se pulsa la tecla space+s: el codigo se
				// encuentra en el metodo keydown

				// cuando el personaje ya ha caido, se vuelve a habiltiar el solido.
				if (jugador.isFalling() && !jugador.overlaps(solid) && jugador.belowOverlaps(solid)) {
					solid.setEnabled(true);
				}
			}

			if (jugador.overlaps(solid) && solid.isEnabled()) {
				Vector2 offset = jugador.preventOverlap(solid);

				if (offset != null) {
					// collided in X direction
					if (Math.abs(offset.x) > Math.abs(offset.y)) {
						jugador.velocityVec.x = 0;
					} else { // collided in Y direction
						jugador.velocityVec.y = 0;
					}
				}
			}
		}

		for (Solid solido : solid) {

			if (solido.isEnabled() && jugador.overlaps(solido)) {
				Vector2 offset = jugador.preventOverlap(solido);

				if (Math.abs(offset.x) > Math.abs(offset.y)) {
					jugador.velocityVec.x = 0;
				} else
					jugador.velocityVec.y = 0;

			}

		}
		for (BaseActor boot : BaseActor.getList(mainStage, "gameClasses.BootsPerk")) {
			if (Launcher.boots == true) {
				boot.remove();
			}
			if (jugador.overlaps(boot)) {
				boot.remove();
				enabledBoots = true;
				Launcher.boots = true;
			}

		}
		for (BaseActor gravedad : BaseActor.getList(mainStage, "gameClasses.GravityPerk")) {
			if (Launcher.gravity == true) {
				gravedad.remove();
			}
			if (jugador.overlaps(gravedad)) {
				gravedad.remove();
				enabledGravity = true;
				Launcher.gravity = true;

			}
		}
		for (BaseActor fbp : BaseActor.getList(mainStage, "gameClasses.FastBulletPerk")) {
			if (Launcher.perk == true) {
				fbp.remove();
			} else if (jugador.overlaps(fbp)) {
				fbp.remove();
				enabledPerk = true;
				Launcher.perk = true;

			}
		}

		if (Launcher.boots == true) {
			bootIcon += 1;
			if (bootIcon == 1) {
				doubleJump = new BaseActor(0, 0, uiStage);
				doubleJump.loadTexture("assets/items/01.png");
				iconBoot.add(doubleJump);
				elements.add(doubleJump);
			}
		}
		if (Launcher.gravity == true) {
			gravityIcon += 1;
			if (gravityIcon == 1) {
				noGravity = new BaseActor(0, 0, uiStage);
				noGravity.loadTexture("assets/items/gravity.png");
				iconZeroG.add(noGravity);
				elements.add(noGravity);
			}
		}
		if (Launcher.perk == true) {
			perkIcon += 1;
			if (perkIcon == 1) {
				speedShot = new BaseActor(0, 0, uiStage);
				speedShot.loadTexture("assets/items/gun.png");
				iconGun.add(speedShot);
				elements.add(speedShot);
			}
		}

		for (BaseActor portalNext : BaseActor.getList(mainStage, "gameClasses.NextPortal")) {

			if (jugador.overlaps(portalNext)) {
				Parameters.end = false;
				Parameters.level += 1;
				start = true;
				nextLevel();

			}
		}
		for (BaseActor portalFin : BaseActor.getList(mainStage, "gameClasses.EndPortal")) {

			if (jugador.overlaps(portalFin)) {
				Parameters.victory = true;
				winScreen();
			}
		}
		for (BaseActor returnPortal : BaseActor.getList(mainStage, "gameClasses.ReturnPortal")) {

			if (jugador.overlaps(returnPortal)) {
				Parameters.end = false;
				Parameters.level -= 1;
				start = false;
				nextLevel();

			}

		}

		if (enabledPerk == true) {

			if (Gdx.input.isKeyJustPressed(Keys.R)) {
				activePerk = true;
			}
			if (activePerk == true) {

				jugador.setBulletCooldown(0.1f);

				timerPerk -= dt;

			}
			if (timerPerk <= 0) {
				jugador.setBulletCooldown(0.5f);
				activePerk = false;
				if (timerPerk == 0) {
					timerCooldown -= dt;
					if (timerCooldown == 0) {
						timerCooldown = 800;
						timerPerk = 55;
					}
				}
			}

		}
		if (enabledGravity == true && jugador.velocityVec.len() < jugador.collisionSpeed) {

			if (Gdx.input.isKeyJustPressed(Keys.Q)) {
				active = true;
			}
			if (active == true) {

				Parameters.setGravedad(250);

				timer -= dt;

			}
			if (timer <= 0) {
				Parameters.setGravedad(650);
				active = false;

			}
			if (jugador.isOnSolid()) {
				timer = 120;

			}

		}
		for (Enemy enemigo : enemy) {
			if (enemigo.isAlive && jugador.overlaps(enemigo)) {
				Vector2 offset = new Vector2();

				if (jugador.overlaps(enemigo)) {
					jugador.dano(10);
				}
				if (Math.abs(offset.x) > Math.abs(offset.y)) {
					jugador.velocityVec.x = 0;
				} else {
					jugador.velocityVec.y = 0;

				}
			}
		}

		if (jugador.healtPoints <= 0) {

			Parameters.end = true;
			Launcher.boots = false;
			Launcher.gravity = false;
			Launcher.perk = false;
			this.gameOver();
		}

		this.vidaRestante.setText("Vida:" + (int) jugador.getHealtPoints());

	}

	/**
	 * Metodo para hacer que el personaje salte. A su vez, este metodo controla que
	 * el personaje pueda atravesar solidos por debajo y por encima. Tambien se
	 * controla que el personaje pueda saltar dos veces si tiene el objeto necesario.
	 */
	
	public boolean keyDown(int keyCode) {
		if (gameOver)
			return false;

		if (keyCode == Keys.SPACE) {
			if (Gdx.input.isKeyPressed(Keys.S)) {
				for (BaseActor actor : BaseActor.getList(mainStage, "gameClasses.Platform")) {
					Platform platform = (Platform) actor;
					if (jugador.belowOverlaps(platform)) {
						platform.setEnabled(false);
					}
				}
			} else if (jugador.isOnSolid()) {
				jugador.salto();
				maxJump = 2;
			} else if (enabledBoots == true) {
				if (!jugador.isOnSolid() && maxJump > 1) {
					jugador.salto();
					maxJump -= 1;
				}
			}

		}
		return false;
	}

	public ArrayList<Solid> getSolidos() {
		return this.solid;
	}

	/**
	 * Metodo que actualiza el sonido de salto.
	 */
	public void JumpSound() {

		jump.play(volumen);
	}

	/**
	 * Metodo que actualiza el sonido de disparo.
	 */
	public void ShotSound() {

		shot.play(volumen);
	}

	/**
	 * Metodo para pasar de nivel.
	 */
	public void nextLevel() {

		BaseGame.setActiveScreen(new LevelScreen(this.enabledBoots, this.enabledGravity, this.enabledPerk,
				this.elements, this.start, this.music));

		this.dispose();

	}

	public void gameOver() {

		this.theme.dispose();
		this.jump.dispose();

		this.dispose();

		BaseGame.setActiveScreen(new GameOverScreen());

	}

	public void winScreen() {

		this.theme.dispose();
		this.jump.dispose();
		this.dispose();

		BaseGame.setActiveScreen(new GameWinScreen());

	}
}

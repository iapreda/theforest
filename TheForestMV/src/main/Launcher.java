package main;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import gameConfiguration.GameScreen;

/**
 * Metodo main. Aqui es donde ocurre la magia.
 * 
 * @author Iulian Preda.
 *
 */
public class Launcher {
	
	static public boolean boots;
	static public boolean gravity;
	static public boolean perk;
	static public boolean start;
	static public boolean last;
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Game myGame = new GameScreen();
		LwjglApplication launcher = new LwjglApplication(myGame, "The Forest",800, 600);
		
	}
}